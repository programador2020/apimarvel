
class Marvel {
    static renderEntity(comic){
        console.log(comic);
        console.log(comic.attributionHTML);
        let template = comic.data.results.map( result =>
            `
                <article class="gallery-item">
                    <h3>${result.title}</h3>
                    <img class="gallery-image" src="${result.thumbnail.path +"."+result.thumbnail.extension}")}" />
                </article>
            `
        ).join('');            
        document.querySelector("#listado").innerHTML += template;
        document.querySelector("footer").innerHTML = comic.attributionHTML;
    }
    static renderFooter(footer) {
        console.log(footer);
    }

    static keysApi(){
        let apiData = {};
        apiData.publicKey = "2de7873b2afc1192c0116368af3ca15c";
        apiData.privateKey = "98040a2ecd7fdb81af3a638a5767b6f14abc0be2";
        apiData.ts = new Date();
        apiData.hash = md5(apiData.ts + apiData.privateKey + apiData.publicKey);
        return(apiData);
    }

    static getApi(){
        const apiData = this.keysApi();
        fetch(`https://gateway.marvel.com/v1/public/comics?ts=${apiData.ts}&apikey=${apiData.publicKey}&hash=${apiData.hash}&limit=50`)
            .then(response => response.json())
            .then(data => this.renderEntity(data))
            //.then(data => this.renderFooter(data))
    }

    static main() {
        this.getApi();                        
    }
}

Marvel.main();